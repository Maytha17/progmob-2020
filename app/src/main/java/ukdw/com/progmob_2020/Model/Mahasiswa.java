package ukdw.com.progmob_2020.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import retrofit2.http.Field;

public class Mahasiswa {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("foto")
    @Expose
    private String imgMhs;

    @SerializedName("nim")
    @Expose
    private String nim;

    @SerializedName("nama")
    @Expose
    private String namaMhs;

    @SerializedName("email")
    @Expose
    private String emailMhs;

    @SerializedName("alamat")
    @Expose
    private String alamatMhs;

    @SerializedName("no_telp")
    @Expose
    private String no_telp;

    @SerializedName("nim_progmob")
    @Expose
    private String nim_progmob;


    public Mahasiswa(String id, String imgMhs, String nim, String namaMhs,
                     String emailMhs, String alamatMhs, String no_telp, String nim_progmob) {
        this.id = id;
        this.imgMhs = imgMhs;
        this.nim = nim;
        this.namaMhs = namaMhs;
        this.emailMhs = emailMhs;
        this.alamatMhs = alamatMhs;
        this.no_telp = no_telp;
        this.nim_progmob = nim_progmob;
    }

    public  String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getImgMhs() {
        return imgMhs;
    }
    public void setImgMhs(String imgMhs){
        this.imgMhs = imgMhs;
    }

    public String getNim() {
        return nim;
    }
    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNamaMhs() {
        return namaMhs;
    }
    public void setNamaMhs(String namaMhs) {
        this.namaMhs = namaMhs;
    }

    public String getEmailMhs() {
        return emailMhs;
    }
    public void setEmailMhs(String emailMhs) {
        this.emailMhs = emailMhs;
    }

    public String getAlamatMhs() {
        return alamatMhs;
    }
    public void setAlamatMhs(String alamatMhs) {
        this.alamatMhs = alamatMhs;
    }

    public String getNo_telp() {
        return no_telp;
    }
    public void setNo_telp(String no_telp) {
        this.no_telp = no_telp;
    }

    public String getNim_progmob() {
        return nim_progmob;
    }
    public void setNim_progmob(String nim_progmob) {
        this.nim_progmob = nim_progmob;
    }
}
