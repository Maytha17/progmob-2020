package ukdw.com.progmob_2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import ukdw.com.progmob_2020.R;

public class SplashScreenActivity extends AppCompatActivity {
    private int loading = 5000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent home = new Intent(SplashScreenActivity.this, LoginActivity.class);
                startActivity(home);
                finish();
            }
        }, loading);
    }
}