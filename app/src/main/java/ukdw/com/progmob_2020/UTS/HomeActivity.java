package ukdw.com.progmob_2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import ukdw.com.progmob_2020.CRUD.MainMhsActivity;
import ukdw.com.progmob_2020.CRUD_Dsn.DosenActivity;
import ukdw.com.progmob_2020.CRUD_MK.MatakuliahActivity;
import ukdw.com.progmob_2020.Model.User;
import ukdw.com.progmob_2020.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        TextView wlc = (TextView) findViewById(R.id.welcome);
        ImageButton dsn = (ImageButton) findViewById(R.id.imgDosen);
        ImageButton mhs = (ImageButton) findViewById(R.id.imgMhs);
        ImageButton mk = (ImageButton) findViewById(R.id.imgMK);
        ImageButton back = (ImageButton) findViewById(R.id.imgBack);

        Bundle b = getIntent().getExtras();
        String textHelp = b.getString("help_string");
        wlc.setText(textHelp);

        dsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, DosenActivity.class);
                startActivity(intent);
            }
        });

        mhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MainMhsActivity.class);
                startActivity(intent);
            }
        });

        mk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MatakuliahActivity.class);
                startActivity(intent);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Intent = new Intent(HomeActivity.this,LoginActivity.class);
                startActivity(Intent);
            }
        });



    }
}