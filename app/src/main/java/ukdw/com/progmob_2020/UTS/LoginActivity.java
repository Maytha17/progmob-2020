package ukdw.com.progmob_2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import ukdw.com.progmob_2020.R;

public class LoginActivity extends AppCompatActivity {
    String isLogin = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        EditText editTextNIM = (EditText) findViewById(R.id.editTextNim);
        EditText editTextPass = (EditText) findViewById(R.id.editTextPassword);
        Button btnLogin = (Button) findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);

                Bundle b = new Bundle();
                b.putString("help_string", editTextNIM.getText().toString());
                intent.putExtras(b);

                if (editTextNIM.getText().toString().length()==0){
                    editTextNIM.setError("Masukan Username");
                }else if
                (editTextPass.getText().toString().length()==0){
                    editTextPass.setError("Masukan Password");
                }else{
                    Toast.makeText(getApplicationContext(), "Berhasil Login" , Toast.LENGTH_LONG).show();
                    startActivity(intent);
                }
            }
        });
    }
}