package ukdw.com.progmob_2020.CRUD_Dsn;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import ukdw.com.progmob_2020.R;
import ukdw.com.progmob_2020.UTS.HomeActivity;

public class DosenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen);

        Button getDsn = (Button) findViewById(R.id.btnGetDosen);
        Button addDsn = (Button) findViewById(R.id.btnAddDosen);
        Button edtDsn = (Button) findViewById(R.id.btnUpdateDsn);
        Button delDsn = (Button) findViewById(R.id.btnDelDosen);
        ImageButton imgBackDsn = (ImageButton) findViewById(R.id.imgBackDsn);

        getDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent = new Intent(DosenActivity.this, DosenGetAllActivity.class) ;
               startActivity(intent);
            }
        });

        addDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DosenActivity.this, DosenAddActivity.class) ;
                startActivity(intent);
            }
        });

        edtDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DosenActivity.this, DosenUpdateActivity.class) ;
                startActivity(intent);
            }
        });

        delDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DosenActivity.this, HapusDosenActivity.class) ;
                startActivity(intent);
            }
        });

        imgBackDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DosenActivity.this, HomeActivity.class) ;
                startActivity(intent);
            }
        });
    }
}
