package ukdw.com.progmob_2020.CRUD_MK;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ukdw.com.progmob_2020.Model.Matakuliah;
import ukdw.com.progmob_2020.R;

public class MatakuliahActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matakuliah);

        Button btnLihatMatkul = (Button)findViewById(R.id.buttonGetAllMatkul);
        Button btnTambahMatkul = (Button)findViewById(R.id.buttonAddMatkul);
        Button btnEditMatkul = (Button) findViewById(R.id.buttonUpdateMatkul) ;
        Button btnDelMatkul = (Button)findViewById(R.id.buttonDelMatkul);

        //action
        btnLihatMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MatakuliahActivity.this, MatakuliahGetAllActivity.class);
                startActivity(intent);
            }
        });

        btnTambahMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MatakuliahActivity.this, MatakuliahAddActivity.class);
                startActivity(intent);
            }
        });

        btnEditMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MatakuliahActivity.this, MatakuliahUpdateActivity.class);
                startActivity(intent);
            }
        });

        btnDelMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MatakuliahActivity.this, HapusMatakuliahActivity.class);
                startActivity(intent);
            }
        });
    }
}
