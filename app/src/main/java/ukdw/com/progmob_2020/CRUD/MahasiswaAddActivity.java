package ukdw.com.progmob_2020.CRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class MahasiswaAddActivity extends AppCompatActivity {

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_add);

        final EditText namaMhs = (EditText) findViewById(R.id.editNamaMhs);
        final EditText nimMhs = (EditText) findViewById(R.id.editNim);
        final EditText alamatMhs = (EditText) findViewById(R.id.editAlamatMhs);
        final EditText emailMhs = (EditText) findViewById(R.id.editEmailMhs);
        Button simpan = (Button) findViewById(R.id.btnUpdateMhs);
        pd = new ProgressDialog(MahasiswaAddActivity.this);

        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon menunggu..");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_mhs(
                        namaMhs.getText().toString(),
                        nimMhs.getText().toString(),
                        alamatMhs.getText().toString(),
                        emailMhs.getText().toString(),"","72180238"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaAddActivity.this, "Data berhasil disimpan", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(MahasiswaAddActivity.this, MahasiswaGetAllActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(MahasiswaAddActivity.this, "Error", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }




}