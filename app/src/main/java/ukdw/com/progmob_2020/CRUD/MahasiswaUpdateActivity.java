package ukdw.com.progmob_2020.CRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class MahasiswaUpdateActivity extends AppCompatActivity {

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_update);

        EditText editNimLama = (EditText)findViewById(R.id.editNimLama);
        EditText editNama = (EditText)findViewById(R.id.editNama);
        EditText editNim = (EditText)findViewById(R.id.editNim);
        EditText editAlamat = (EditText)findViewById(R.id.editAlamat);
        EditText editEmail = (EditText)findViewById(R.id.editEmail);
        Button btnUpdate = (Button)findViewById(R.id.btnUpdateMhs);
        pd = new ProgressDialog(MahasiswaUpdateActivity.this);

        Intent data = getIntent();
        if(data.getExtras() != null){
            editNama.setText(data.getStringExtra("nama"));
            editNim.setText(data.getStringExtra("nim"));
            editNimLama.setText(data.getStringExtra("nim"));
            editAlamat.setText(data.getStringExtra("alamat"));
            editEmail.setText(data.getStringExtra("email"));
        }

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_mhs(
                        editNama.getText().toString(),
                        editNim.getText().toString(),
                        editNimLama.getText().toString(),
                        editAlamat.getText().toString(),
                        editEmail.getText().toString(),
                        "Kosongkan saja",
                        "72180238"
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this, "Data berhasil diubah", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(MahasiswaUpdateActivity.this, MainMhsActivity.class);
                        startActivity(intent);
                    }
                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this, "Data tidak berhasil diubah", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}