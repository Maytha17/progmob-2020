package ukdw.com.progmob_2020.Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.progmob_2020.Adapter.MahasiswaRecyclerAdapter;
import ukdw.com.progmob_2020.Model.Mahasiswa;
import ukdw.com.progmob_2020.R;

public class RecyclerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        RecyclerView rv = (RecyclerView) findViewById(R.id.rvLatihan);
        MahasiswaRecyclerAdapter mahasiswaRecyclerAdapter;

        // data dummy
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        // generate data mahasiswa
//        Mahasiswa m1 = new Mahasiswa("Maytha Walvinata Sitio", "72180238", "085252996577");
//        Mahasiswa m2 = new Mahasiswa("Nova Sari Padatuan", "72180236", "085252996787");
//        Mahasiswa m3 = new Mahasiswa("Jessy Friska Sitinjak", "72180248", "082252995577");
//        Mahasiswa m4 = new Mahasiswa("Abimael Sibuea", "72180239", "085252996566");
//        Mahasiswa m5 = new Mahasiswa("Ade Sembiring", "72180240", "085252236577");
//
//        mahasiswaList.add(m1);
//        mahasiswaList.add(m2);
//        mahasiswaList.add(m3);
//        mahasiswaList.add(m4);
//        mahasiswaList.add(m5);

        mahasiswaRecyclerAdapter = new MahasiswaRecyclerAdapter(RecyclerActivity.this);
        mahasiswaRecyclerAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(RecyclerActivity.this));
        rv.setAdapter(mahasiswaRecyclerAdapter);
    }
}