package ukdw.com.progmob_2020.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ukdw.com.progmob_2020.CRUD.MahasiswaUpdateActivity;
import ukdw.com.progmob_2020.Model.Mahasiswa;
import ukdw.com.progmob_2020.R;

public class MahasiswaCRUDRecyclerAdapter extends RecyclerView.Adapter<MahasiswaCRUDRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<Mahasiswa> mahasiswaList;

    public MahasiswaCRUDRecyclerAdapter(Context context) {
        this.context = context;
        mahasiswaList = new ArrayList<>();
    }

    public MahasiswaCRUDRecyclerAdapter(List<Mahasiswa> mahasiswaList) {
        this.mahasiswaList = mahasiswaList;
    }

    public List<Mahasiswa> getMahasiswaList() {
        return mahasiswaList;
    }

    public void setMahasiswaList(List<Mahasiswa> mahasiswaList) {
        this.mahasiswaList = mahasiswaList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview,parent,false);
        return new ViewHolder(v);
        }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Mahasiswa m = mahasiswaList.get(position);

        holder.tvNama.setText(m.getNamaMhs());
        holder.tvNim.setText(m.getNim());
       // holder.tvNoTelp.setText(m.getNo_telp());
        holder.tvAlamatMhs.setText(m.getAlamatMhs());
        holder.tvEmailMhs.setText(m.getEmailMhs());
        holder.m = m;
    }

    @Override
    public int getItemCount() {
        return mahasiswaList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNama, tvNim, tvNoTelp, tvAlamatMhs, tvEmailMhs;
        private RecyclerView rvGetMhsAll;
        Mahasiswa m;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvNim = itemView.findViewById(R.id.tvNim);
            // tvNoTelp = itemView.findViewById(R.id.tvNoTelp);
            tvAlamatMhs = itemView.findViewById(R.id.tvAlamatMhs);
            tvEmailMhs = itemView.findViewById(R.id.tvEmailMhs);
            rvGetMhsAll = itemView.findViewById(R.id.rvGetMhsAll);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(itemView.getContext(), MahasiswaUpdateActivity.class);
                    intent.putExtra("nim",m.getNim());
                    intent.putExtra("nama",m.getNamaMhs());
                    intent.putExtra("alamat",m.getAlamatMhs());
                    intent.putExtra("email",m.getEmailMhs());

                    itemView.getContext().startActivity(intent);
                }
            });
        }
    }
}
